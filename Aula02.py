# Tipos primitivos

# String    -> str  -> Frases, conjuntos de caracteres, sempre entre aspas.
# Integer   -> int  -> Numeros inteiros, sem aspas.
# Float     -> float-> Numeros decimais, numeros reais, numeros com ponto, sem aspas.
# Boolean   -> bool -> True

nome    = "Gustavo Fumagalli"   #String
peso    = "80"                  #String
idade   = 31                    #Integer
altura  = 1.80                  #Float
moreno  = True                  #Boolean

#==========================================

numero1 = 30
numero2 = 15

numero3 = numero1 + numero2
#print(numero3)

#=============================================================

#idade = input("Qual a sua idade?")

#idade = int(idade)

# print(idade)

#=================

#nome = "Gustavo P Fumagalli".replace("p", "g")
#print(nome)
#print(nome.islower())

#======================

# Estruturas de decisão

#idade = int(input("Qual a sua idade? "))

#if idade > 17:
#    print("Você pode comprar bebidas")

#elif idade >15:
#    print("Você pode entrar mas não comprar bebidas")
#else:
#    print("Você é muito jovem para entrar.")


# =========================================

# Estruturas de repetição (do loop)

# FOR e WHILE
    
# contador = 10
# while contador != 0:
#     print("Estou contando...")   
#     contador = contador - 1
#     print(contador)

# while True:
#       resposa = input("Quer parar o programa? [s/n]") 
#       if resposa == "s":
#        break 
      
#       resposta2 = input("Quer continuar a execução atual? [S/N]")
#       if resposta2 == "s"
#          continue
      
#       while True:
#           print("teste")
#           break
      
#=============================

for x in range(0, 10):
    print("imprimindo mensagem")
    print(x)

